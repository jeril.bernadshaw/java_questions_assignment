package com.Java;

public class Question1DemonstratePoly {
	public static void main(String[] args) {
        Question1Car car = new Question1Santro();
        car.drive();
        car.stop();
        
        ((Question1Santro) car).remotestart();
        Question1BasicCar basicCar = (Question1BasicCar) car;
        basicCar.gearchange();
        basicCar.music();
    }
}


